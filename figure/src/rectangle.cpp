#include <sstream>
#include "rectangle.h"
#include <string>

using namespace std;

string rectangle::description() const
{
	return "rectangle"; //return le nom  de la  figure
}

string rectangle::toString() const
{
	ostringstream oss;
	oss << longueur << " " <<largeur; // attributs spécifique
	return figure::toString() + " " + oss.str();
}
