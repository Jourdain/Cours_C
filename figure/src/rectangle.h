#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "figure.h"
#include <string>
#include <cmath>
#include <iostream>

using namespace std;
//CLASS HERITAGE  RECTAGLE

class rectangle : public figure  //HERITE

{
	protected:
		double longueur, largeur;


	public:
		rectangle( double longueur, double largeur, double x = 0, double y = 0)
		:figure(x, y, 0), longueur(longueur), largeur(largeur) {}
		~rectangle() {}
		double perimettre() {return (longueur + largeur) /2; } //CALCUL PERIMETRE
		double aire()	    {return longueur * largeur; } //CALCUL AIRE
		string description () const;
		string toString() const;
};

#endif
