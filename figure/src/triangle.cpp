#include "triangle.h"

#include <sstream>
#include <string>


using namespace std;

string triangle::description() const
{
	return "triangle"; //return le nom de la figure
}

string triangle::toString()  const
{
	ostringstream oss;
	oss << base << " " << hauteur; //attributs spécifique
	return figure::toString() + " " + oss.str();
}
