#ifndef CARRE_H
#define CARRE_H

#include <cmath>
#include "figure.h"
#include <string> 

//heritage 
using namespace std;


class carre : public figure
{ 
	protected:
		double largeur;
	public:
		carre(double largeur, double x = 0, double y = 0)
		: figure(x, y, 0), largeur(largeur) {}
		~carre() {}
		double perimettre() {return largeur * 4; } //calcul le perimettre
		double aire()	    { return largeur *  largeur; } //calcul l'aire
		string description() const;
		string toString() const;
};

#endif
