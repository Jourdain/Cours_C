#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "figure.h"
#include <string>
#include <cmath>
#include <iostream>


using namespace std;

class triangle : public figure 
{
	protected: 
		double base, hauteur;

	public:
		triangle(double base, double hauteur, double x = 0, double y = 0)
		: figure (x, y, 0), base(base), hauteur(hauteur) {}
		~triangle() {}
		double perimettre() { return sqrt(base * base + hauteur * hauteur) + base + hauteur; }
		double aire() {return (base * hauteur)/2; };
		string description() const;
		string toString() const;
};

#endif

