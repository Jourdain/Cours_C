#include "carre.h"

#include <sstream>
#include <string>

using namespace std;

string carre::description() const
{
	return "carre";
}

string carre::toString() const 
{
	ostringstream oss;
	oss << largeur; 
	return figure::toString() + " "+ oss.str();
}

