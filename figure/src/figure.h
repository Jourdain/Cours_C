#ifndef FIGURE_H
#define FIGURE_H

#include <string>

using namespace std;

//CLASS PARENT FIGURE
class figure 
{
        public:
		figure( double x = 0, double y =0, double z = 0) : x(x), y(y), z(z){} 
	  	virtual ~figure() {}
		virtual double perimettre() {return 0; }//fonction pour calculer le perimetre
		virtual double aire() {return 0; }//fonction pour calculer l'aire
		string description() const;//fonction décrire ici rest une constante
		string toString() const;// fonction constante

	protected:
		double x, y, z; // INITIALISE LES CARACTERISTIQUE DUNE FIGURE 
};

#endif
