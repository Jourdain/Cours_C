#include "cercle.h"


#include <sstream>
#include <string>

using namespace std;


string cercle::description() const
{
	return "cercle"; //renvoi le nom de la figure
}

string cercle::toString() const
{
	ostringstream oss;
	oss << rayon << " " << pi; //attribut spécifique
	return figure::toString() + " " + oss.str();
}
