#ifndef CERCLE_H
#define CERCLE_H

#include "figure.h"
#include <cmath>
#include <string>
#include <iostream>
//CLASS FILLE CERCLE

using namespace std;

class cercle: public figure //RECUPERE LES FONCTION DE LA CLASS PARENT (HERITAGE) 

{  
   public:
	cercle(double rayon, double pi, double x = 0, double y = 0)
	:figure(x, y, 0), rayon(rayon), pi(pi) {}
	~cercle() {}
	double perimettre() {return 2 * pi * rayon; } //SPECIFIER LA FONCTION PERIMETTRE
	double aire() 	    {return  pi * (rayon * rayon); } //PAREIL POUR L'AIRE
	string description() const;
	string toString() const;
        
	protected:
		double rayon, pi;
};

#endif
