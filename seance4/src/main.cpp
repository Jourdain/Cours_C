#include <vector>

#include <iostream>

using namespace std;

int main ()
 
{
	//creation of 3 dim table
	vector <int> tab(3);
	tab[0] = 3;
	tab[1] = 4;
	tab[2] = 5;

	tab.erase(tab.begin()+1); //delete the second case of the table which take the next number
	tab.push_back(1000); //add a other cas at end of the table
	cout << tab[0] << " " << tab[1] << " "<< tab[2] << " " <<tab[3] << endl;
	cout << tab.size() << endl; //display the size of the table

	return 0;
}
